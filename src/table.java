
public class table {
	private char[][] table = {
			{'-', '-', '-'},
			{'-', '-', '-'},
			{'-', '-', '-'}
	};
	private player x;
	private player o;
	private player winner;
	private player current;
	private int turncount;
	
	
	public table(player x,player o) {
		this.x = x;
		this.o = o;
		current = x;
		winner = null;
		turncount = 0;
	}
	

	public char[][] gettable(){
		return table;
	}
	public player getCurrent() {
		return current;
	}

	public boolean setTable(int row, int col) {
		if(table[row][col] == '-') {
			table[row][col] = current.getName();
			return true;
		}
		return false;
	}
	private boolean checkrow(int row) {
		for(int col=0 ; col<table[row].length;col++) {
			if(table[row][col] != current.getName()) {
				return false;
			}
		}
		return true;
	}
	private boolean checkrow() {
		if(checkrow(0) || checkrow(1) || checkrow(2)) {
			return true;
		}
		return false;
	}
	private boolean checkcol(int col) {
		for(int row=0 ; row<table[col].length;row++) {
			if(table[row][col] != current.getName()) {
				return false;
			}
		}
		return true;
	}
	private boolean checkcol() {
		if(checkcol(0) || checkcol(1) || checkcol(2)) {
			return true;
		}
		return false;
	}
	private boolean checkx1() {
		for (int i = 0; i < table.length; i++) {
			if(table[i][i]!=current.getName()) {
				return false;
			}
		}
		return true;
	}
	private boolean checkx2() {
		for (int i = 0; i < table.length; i++) {
			if(table[2-i][i]!=current.getName()) {
				return false;
			}
		}
		return true;
	}
	private boolean checkdraw() {
		if(turncount == 8) {
			x.setDraw();
			o.setDraw();
			return true;
		}
		return false;
	}
	
	private boolean checkwin() {
		if(checkrow()  || checkcol() || checkx1() || checkx2() ) {
			winner = current;
			if(current == x) {
				x.setWin();
				o.setLose();
			}else {
				o.setWin();
				x.setLose();
			}
			return true;
		} 
		return false;
	}
	public player getWinner() {
		return winner;
	}
	
	public boolean isFinish() {
		if(checkwin()) {
			return true;
		}
		if(checkdraw()) {
			return true;
		}
		return false;
	}
	
	public void Switch() {
	if(current == x) {
		current = o;
	}else {
		current = x;
	}
	turncount++;
	}
		

}
