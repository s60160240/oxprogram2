import java.util.Scanner;

public class game {

	private player x;
	private player o;
	private table board;

	public game() {
		x = new player('x');
		o = new player('o');
		board = new table(x, o);
	}

	public void playone() {
		board = new table(x, o);
		showwelcome();
		while (true) {
			showtable();
			showturn();
			input();
			if (board.isFinish()) {
				break;
			}
			board.Switch();
		}
		showtable();
		showwinner();
		showstart();

	}

	public void play() {
		while (true) {
			playone();
		}
	}

	private void showstart() {

		System.out.println(x.getName() + " win,draw,lose" + x.getWin() + "," + x.getDraw() + "," + x.getLose());
		System.out.println(o.getName() + " win,draw,lose" + o.getWin() + "," + o.getDraw() + "," + o.getLose());
	}

	private void showwinner() {
		if (board.getWinner() != null) {
			player player = board.getWinner();
			System.out.println(player.getName() + " win..");
		}else {
			System.out.println("DRAW!!");
		}
	}

	public void showwelcome() {
		System.out.println("Welcome to ox game.");
	}

	private void showtable() {
		char[][] table = board.gettable();
		System.out.println("  1 2 3");
		for (int i = 0; i < table.length; i++) {
			System.out.print((i + 1));
			for (int j = 0; j < table.length; j++) {
				System.out.print(" " + table[i][j]);
			}
			System.out.println();
		}
	}

	private void showturn() {
		player Player = board.getCurrent();
		System.out.println(Player.getName() + " turn. ");
	}

	private void input() {
		Scanner kb = new Scanner(System.in);
		while (true) {
			try {
				System.out.print("Please input row col: ");
				String input = kb.nextLine();
				String[] str = input.split(" ");
				if (str.length != 2) {
					System.out.println("Please input row col[1-3] (ex: 1 2)");
					continue;
				}
				int row = Integer.parseInt(str[0]) - 1;
				int col = Integer.parseInt(str[1]) - 1;
				if (board.setTable(row, col) == false) {
					System.out.println("Table is not empty!!");
					continue;
				}
				break;
			} catch (Exception e) {
				System.out.println("Please input row col[1-3] (ex: 1 2)");
				continue;
			}

		}

	}

}
